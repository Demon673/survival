var heroIndex = -1;
var ItemContainersCallback = -1;
var InventoryPanel = $("#Inventory");
InventoryPanel.RemoveAndDeleteChildren();
var EquipmentsPanel = $("#Equipments");
EquipmentsPanel.RemoveAndDeleteChildren();
var BackpackPanel = $("#Backpack");
BackpackPanel.RemoveAndDeleteChildren();
var ChestsPanel = $("#Chests");
ChestsPanel.RemoveAndDeleteChildren();

function Update()
{
	$.Schedule(0, Update);
	var localHeroIndex = Players.GetLocalPlayerPortraitUnit();
	if (localHeroIndex != -1 && heroIndex != localHeroIndex && Entities.IsHero(localHeroIndex))
	{
		heroIndex = localHeroIndex;
		SubscribeHeroContainersEvent(GetUnitParentEntity(heroIndex));
		CustomNetTables.SubscribeNetTableListener("unitEntities",
			function(tableName, unitIndex, unitEntity)
			{
				if (unitIndex == String(heroIndex))
				{
					SubscribeHeroContainersEvent(unitEntity);
				}
			}
		);
	}
}

function OnInventoryChanged(inventoryIndex)
{
	var inventory = GetContainerByIndex(inventoryIndex);
	var row = inventory.row;
	var colm = inventory.colm;
	var list = inventory.list;
	var capacity = row*colm;
	for (var y = 1; y <= row; y++)
	{
		for (var x = 1; x <= colm; x++)
		{
			var slotIndex = x + (y-1)*colm;
			var slotPanel = InventoryPanel.FindChildTraverse("InventorySlot_"+slotIndex);
			if (slotPanel == undefined || slotPanel == null)
			{
				slotPanel = $.CreatePanel("Panel", InventoryPanel, "InventorySlot_"+slotIndex);
			}
			var px = (3*x+62*(x-1));
			var py = (2*y+47*(y-1));
			slotPanel.style.marginLeft = px +"px";
			slotPanel.style.marginTop = py +"px";
			InitSlot(slotPanel, list[String(slotIndex)], inventoryIndex, slotIndex);
		}
	}
	for (var i = capacity+1; i <= InventoryPanel.GetChildCount(); i++)
	{
		var slotPanel = InventoryPanel.FindChildTraverse("InventorySlot_"+i);
		slotPanel.DeleteAsync(-1);
		RemoveSlot(slotPanel);
	}
}

function OnEquipmentsChanged(equipmentsIndex)
{
	var equipments = GetContainerByIndex(equipmentsIndex);
	var row = equipments.row;
	var colm = equipments.colm;
	var list = equipments.list;
	var capacity = row*colm;
	for (var y = 1; y <= row; y++)
	{
		for (var x = 1; x <= colm; x++)
		{
			var slotIndex = x + (y-1)*colm;
			var slotPanel = EquipmentsPanel.FindChildTraverse("EquipmentsSlot_"+slotIndex);
			if (slotPanel == undefined || slotPanel == null)
			{
				slotPanel = $.CreatePanel("Panel", EquipmentsPanel, "EquipmentsSlot_"+slotIndex);
			}
			var px = (3*x+62*(x-1));
			var py = (2*y+47*(y-1));
			slotPanel.style.marginLeft = px +"px";
			slotPanel.style.marginTop = py +"px";
			InitSlot(slotPanel, list[String(slotIndex)], equipmentsIndex, slotIndex);
		}
	}
	for (var i = capacity+1; i <= EquipmentsPanel.GetChildCount(); i++)
	{
		var slotPanel = EquipmentsPanel.FindChildTraverse("EquipmentsSlot_"+i);
		slotPanel.DeleteAsync(-1);
		RemoveSlot(slotPanel);
	}
}

function OnBackpackChanged(backpackIndex)
{
	var backpack = GetContainerByIndex(backpackIndex);
	var row = backpack.row;
	var colm = backpack.colm;
	var list = backpack.list;
	var capacity = row*colm;
	for (var y = 1; y <= row; y++)
	{
		for (var x = 1; x <= colm; x++)
		{
			var slotIndex = x + (y-1)*colm;
			var slotPanel = BackpackPanel.FindChildTraverse("BackpackSlot_"+slotIndex);
			if (slotPanel == undefined || slotPanel == null)
			{
				slotPanel = $.CreatePanel("Panel", BackpackPanel, "BackpackSlot_"+slotIndex);
			}
			var px = (3*x+62*(x-1));
			var py = (2*y+47*(y-1));
			slotPanel.style.marginLeft = px +"px";
			slotPanel.style.marginTop = py +"px";
			InitSlot(slotPanel, list[String(slotIndex)], backpackIndex, slotIndex);
		}
	}
	for (var i = capacity+1; i <= BackpackPanel.GetChildCount(); i++)
	{
		var slotPanel = BackpackPanel.FindChildTraverse("BackpackSlot_"+i);
		RemoveSlot(slotPanel);
	}
} 

function OnChestChanged(id, chestIndex)
{
	var chestPanel = ChestsPanel.FindChildTraverse("Chest_"+id)
	if (chestPanel == undefined || chestPanel == null)
	{
		return;
	}
	var chest = GetContainerByIndex(chestIndex);
	var row = chest.row;
	var colm = chest.colm;
	var list = chest.list;
	var capacity = row*colm;
	for (var y = 1; y <= row; y++)
	{
		for (var x = 1; x <= colm; x++)
		{
			var slotIndex = x + (y-1)*colm;
			var slotPanel = chestPanel.FindChildTraverse("ChestSlot_"+slotIndex);
			if (slotPanel == undefined || slotPanel == null)
			{
				slotPanel = $.CreatePanel("Panel", chestPanel, "ChestSlot_"+slotIndex);
			}
			var px = (3*x+62*(x-1));
			var py = (2*y+47*(y-1));
			slotPanel.style.marginLeft = px +"px";
			slotPanel.style.marginTop = py +"px";
			InitSlot(slotPanel, list[String(slotIndex)], chestIndex, slotIndex);
		}
	}
	for (var i = capacity+1; i <= chestPanel.GetChildCount(); i++)
	{
		var slotPanel = chestPanel.FindChildTraverse("ChestSlot_"+i);
		RemoveSlot(slotPanel);
	}
}

function SubscribeHeroContainersEvent(unitEntity)
{
	var openChests = unitEntity.openChests;
	var n = 0;
	for (var id in openChests)
	{
		var chestIndex = openChests[id];
		
		var chestPanel = ChestsPanel.FindChildTraverse("Chest_"+id);
		if (chestPanel == undefined || chestPanel == null)
		{
			chestPanel = $.CreatePanel("Panel", ChestsPanel, "Chest_"+id);
			chestPanel.AddClass("Chest");
		}
		chestPanel.AddClass("Show");
		OnChestChanged(id, chestIndex);
		n++;
	}
	for (var i = n+1; i <= ChestsPanel.GetChildCount(); i++)
	{
		var chestPanel = ChestsPanel.FindChildTraverse("Chest_"+i);
		for (var j = 1; j <= chestPanel.GetChildCount(); j++)
		{
			var slotPanel = chestPanel.FindChildTraverse("ChestSlot_"+j);
			RemoveSlot(slotPanel);
		}
		chestPanel.DeleteAsync(-1);
	}

	var inventoryIndex = unitEntity.inventory;
	var equipmentsIndex = unitEntity.equipments;
	var backpackIndex = unitEntity.backpack;

	OnInventoryChanged(inventoryIndex);
	OnEquipmentsChanged(equipmentsIndex);
	OnBackpackChanged(backpackIndex);

	if (ItemContainersCallback != -1)
	{
		CustomNetTables.UnsubscribeNetTableListener(ItemContainersCallback);
		ItemContainersCallback = -1;
	}
	ItemContainersCallback = CustomNetTables.SubscribeNetTableListener("itemContainers",
		function(tableName, containerIndex, data)
		{
			if (containerIndex == String(inventoryIndex))
			{
				OnInventoryChanged(containerIndex);
			}
			if (containerIndex == String(equipmentsIndex))
			{
				OnEquipmentsChanged(containerIndex);
			}
			if (containerIndex == String(backpackIndex))
			{
				OnBackpackChanged(containerIndex);
			}
			for (var id in openChests)
			{
				var chestIndex = openChests[id];
				if (containerIndex == String(chestIndex))
				{
					OnChestChanged(id, containerIndex);
				}
			}
		}
	);

}

(function()
{
	for (var i = 0; i < DotaDefaultUIElement_t.DOTA_DEFAULT_UI_ELEMENT_COUNT; i++)
	{
		GameUI.SetDefaultUIEnabled(i, true);
	}
	Update();
})();