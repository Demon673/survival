var g_bInBossIntro = false;
var g_nBossCameraEntIndex = -1;
var g_flCameraDesiredOffset = 128.0;
var g_flAdditionalCameraOffset = 0.0;
var g_flMaxLookDistance = 900.0;
var g_nCachedCameraEntIndex = -1;
var g_fCameraDistance = 900;

function UpdateCameraOffset()
{
	var localCamFollowIndex = Players.GetPlayerHeroEntityIndex( Players.GetLocalPlayer() );
	//handle spectators
	if ( Players.IsLocalPlayerInPerspectiveCamera() )
	{
		localCamFollowIndex = Players.GetPerspectivePlayerEntityIndex();
	}

	if ( g_nBossCameraEntIndex !== -1 )
	{
		localCamFollowIndex = g_nBossCameraEntIndex;
	}
	if ( localCamFollowIndex !== -1 )
	{
		var vDesiredLookAtPosition = Entities.GetAbsOrigin( localCamFollowIndex );
		var vLookAtPos = GameUI.GetCameraLookAtPosition();
		var flCurOffset = GameUI.GetCameraLookAtPositionHeightOffset();
		var flCameraRawHeight = vLookAtPos[2] - flCurOffset;
		var flEntityHeight = vDesiredLookAtPosition[2];
		vDesiredLookAtPosition[1] = vDesiredLookAtPosition[1] - 180.0;

		var bMouseWheelDown = GameUI.IsMouseDown( 2 );
		if ( bMouseWheelDown )
		{
			var vScreenWorldPos = GameUI.GetScreenWorldPosition( GameUI.GetCursorPosition() );
			if ( vScreenWorldPos !== null )
			{
				var vToCursor = [];
				vToCursor[0] = vScreenWorldPos[0] - vDesiredLookAtPosition[0];
				vToCursor[1] = vScreenWorldPos[1] - vDesiredLookAtPosition[1];
				vToCursor[2] = vScreenWorldPos[2] - vDesiredLookAtPosition[2];
				vToCursor = Game.Normalized( vToCursor );
				var flDistance = Math.min( Game.Length2D( vScreenWorldPos, vDesiredLookAtPosition ), g_flMaxLookDistance );
				vDesiredLookAtPosition[0] = vDesiredLookAtPosition[0] + vToCursor[0] * flDistance;
				vDesiredLookAtPosition[1] = vDesiredLookAtPosition[1] + vToCursor[1] * flDistance;
				vDesiredLookAtPosition[2] = vDesiredLookAtPosition[2] + vToCursor[2] * flDistance;
			}
		}

		var flHeightDiff = flCameraRawHeight - flEntityHeight;
		var flNewOffset = g_flCameraDesiredOffset - flHeightDiff + 50;
		var key = 0;
		// var bossData = CustomNetTables.GetTableValue("boss", key.toString());
		var flAdditionalOffset = 0.0;
		if ( typeof( bossData ) != "undefined" )
		{
			var bShowBossHP = bossData["boss_hp"] == 0 ? false : true;
			if ( bShowBossHP )
			{
				flAdditionalOffset = 100.0;
			}
		}

		var t = Game.GetGameFrameTime() / 1.5;
		if ( t > 1.0 ) { t = 1.0; }

		g_flAdditionalCameraOffset = g_flAdditionalCameraOffset * t + flAdditionalOffset * ( 1.0 - t );
		flNewOffset = flNewOffset + g_flAdditionalCameraOffset;

		var flLerp = 0.05;
		if ( bMouseWheelDown )
		{
			flLerp = 0.1;
		}
		if ( g_nCachedCameraEntIndex !== localCamFollowIndex )
		{
			flLerp = 1.5;
		}

		GameUI.SetCameraTargetPosition(vDesiredLookAtPosition, flLerp);
		GameUI.SetCameraLookAtPositionHeightOffset( flNewOffset );

		g_nCachedCameraEntIndex = localCamFollowIndex;
	}
	else
	{
		GameUI.SetCameraLookAtPositionHeightOffset( 0.0 );
	}
}

(function CameraThink() {
	GameUI.SetCameraDistance(g_fCameraDistance);
	if ( Game.GetState() < DOTA_GameState.DOTA_GAMERULES_STATE_POST_GAME )
	{
		UpdateCameraOffset();
	}
	$.Schedule(0, CameraThink);
})();