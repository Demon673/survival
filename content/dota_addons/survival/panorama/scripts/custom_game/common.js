//init
function DeepPrint(obj){return GameUI.CustomUIConfig().alertObj(obj);}

function GetCursorEntity(){return GameUI.CustomUIConfig().GetCursorEntity();}

function GetHUDSeed(){return GameUI.CustomUIConfig().GetHUDSeed();}

function CorrectPositionValue(value){return GameUI.CustomUIConfig().CorrectPositionValue(value);}

function StartDrag(panel){return GameUI.CustomUIConfig().StartDrag(panel);}

function EndDrag(){return GameUI.CustomUIConfig().EndDrag();}

function ErrorMessage(msg){return GameUI.CustomUIConfig().ErrorMessage(msg);}

function GetUnitParentEntity(unit){return GameUI.CustomUIConfig().GetUnitParentEntity(unit);}

function GetContainerByIndex(index){return GameUI.CustomUIConfig().GetContainerByIndex(index);}

function GetItemDataByIndex(index){return GameUI.CustomUIConfig().GetItemDataByIndex(index);}

//custom
Entities.HasBuff = function(nEntityIndex, pszBuffName)
{
	var nBuffs = Entities.GetNumBuffs(nEntityIndex);
	for (var i = 0; i < nBuffs; ++i)
	{
		var buff_serial = Entities.GetBuff(nEntityIndex, i);

		if (buff_serial == -1) continue;

		if (Buffs.GetName(nEntityIndex, buff_serial) == pszBuffName)
		{
			return true;
		}
	}
	return false;
}

Entities.IsChest = function(entityIndex)
{
	if (entityIndex != -1 && GetUnitParentEntity(entityIndex) != undefined && GetUnitParentEntity(entityIndex).chest != undefined)
	{
		return true;
	}
	return false;
}

Entities.HasModifier = function(entIndex, modifierName)
{
	var nBuffs = Entities.GetNumBuffs(entIndex);
	for (var i = 0; i < nBuffs; i++)
	{
		if (Buffs.GetName(entIndex, Entities.GetBuff(entIndex, i)) == modifierName)
		{
			return true;
		}
	}
	return false;
};

function Print(msg)
{
	$.Msg(msg);
}
print = Print;

//prototype
Array.prototype.indexOf = function(val)
{
	for (var i = 0; i < this.length; i++)
	{
		if (this[i] == val) return i;
	}
	return -1;
}

Array.prototype.remove = function(val)
{
	var index = this.indexOf(val);
	if (index > -1)
	{
		this.splice(index, 1);
	}
}

// var variables = "";

// for (var name in this)
// {
// 	variables += name + "\n";
// }
// $.Msg(variables);
// DeepPrint(Game);