var CONSUME_EVENT = true;
var CONTINUE_PROCESSING_EVENT = false;

function OnRightPressed()
{
    var iPlayerID = Players.GetLocalPlayer();
    var selectedEntities = Players.GetSelectedEntities( iPlayerID );
    var mainSelected = Players.GetLocalPlayerPortraitUnit();
    var mainSelectedName = Entities.GetUnitName(mainSelected);
    var targetIndex = GetCursorEntity();
    var cursor = GameUI.GetCursorPosition();
	var worldPosition = GameUI.GetScreenWorldPosition(cursor);
    var pressedShift = GameUI.IsShiftDown();
    var bMessageShown = false;


	//Chest right click
	if (Entities.IsChest(targetIndex) && Entities.IsControllableByPlayer(mainSelected, iPlayerID))
	{
		if (!Game.IsGamePaused())
		{
			Game.PrepareUnitOrders(
				{
					UnitIndex: mainSelected,
					OrderType: dotaunitorder_t.DOTA_UNIT_ORDER_MOVE_TO_TARGET,
					TargetIndex: targetIndex,
					AbilityIndex: -2,
				}
			);
			var params = {
				unitIndex : mainSelected,
				chestIndex : targetIndex,
			};
			GameEvents.SendCustomGameEventToServer("open_chest", params);

			return CONSUME_EVENT;
		}
	}

	//Tree right click
	if ((targetIndex == -1 || targetIndex == mainSelected) && Entities.HasModifier(mainSelected, "modifier_cut_trees") && worldPosition != null)
	{
		if (!Game.IsGamePaused())
		{
			var params = {
				unitIndex : mainSelected,
				position : worldPosition,
			};
			GameEvents.SendCustomGameEventToServer("check_tree_click", params);
		}
	}

	return CONTINUE_PROCESSING_EVENT;
}

GameUI.SetMouseCallback(function(eventName, arg)
{
	if (GameUI.GetClickBehaviors() !== CLICK_BEHAVIORS.DOTA_CLICK_BEHAVIOR_NONE)
	{
		return CONTINUE_PROCESSING_EVENT;
	}

	// Right-click
	if (arg == 1)
	{
		if (eventName == "pressed" || eventName == "doublepressed")
		{
			return OnRightPressed();
		}
	}

	if (eventName === "pressed")
	{
		// Left-click
		if ( arg === 0 )
		{
		}


		// Middle-click
		if ( arg === 2 )
		{
		}
	}

	// if (eventName === "wheeled")
	// {
	// }

	// if (eventName === "doublepressed")
	// {
	// }
	return CONTINUE_PROCESSING_EVENT;
});
