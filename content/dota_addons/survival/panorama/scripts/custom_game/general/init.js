(function(){
	GameUI.CustomUIConfig().alertObj = function(obj,name,str)
	{
		var output = "";
		if (name == null)
		{
			name = toString(obj);
		}
		if (str == null)
		{
			str = "";
		}
		$.Msg(str+name+"\n"+str+"{")
		for(var i in obj){  
			var property=obj[i];
			if (typeof(property) == "object")
			{
				GameUI.CustomUIConfig().alertObj(property,i,str+"\t");
			}
			else
			{
				output = i + " = " + property + "\t(" + typeof(property) +")";
				$.Msg(str+"\t"+output);
			}
		}
		$.Msg(str+"}");
	}

	GameUI.CustomUIConfig().GetCursorEntity = function()
	{
		var targets = GameUI.FindScreenEntities(GameUI.GetCursorPosition());
		var targets1 = targets.filter(function(e)
			{
				return e.accurateCollision;
			});
		var targets2 = targets.filter(function(e)
			{
				return !e.accurateCollision;
			});
		targets = targets1;
		if (targets1.length == 0)
		{
			targets = targets2;
		}
		if (targets.length == 0)
		{
			return -1;
		}
		return targets[0].entityIndex;
	}

	GameUI.CustomUIConfig().GetPlayerOwner = function(unit)
	{
		for(var i = 0; i < Players.GetMaxPlayers(); i++)
		{
			if (Entities.IsControllableByPlayer(unit, i))
			{
				return i;
			}
		}
		return -1;
	}

	GameUI.CustomUIConfig().SetLocalCameraPitch = function(Pitch)
	{
		GameUI.SetCameraPitchMin(Pitch+360);
		GameUI.SetCameraPitchMax(Pitch+360);
	}

	GameUI.CustomUIConfig().SetMainStateUIVisible = function(bool,whitelist)
	{
		var panel = $.GetContextPanel();
		// $.Msg(panel.paneltype)
		while( panel.paneltype != "DOTACustomUIRoot")
		{
			 panel =  panel.GetParent();
		}
		var t = 0;
		while(panel.GetChild(t).id != "CustomUIContainer_Hud")
			t++; 
		// $.Msg(panel.GetChild(t))
		panel = panel.GetChild(t)
		for (var i = 0; i < panel.GetChildCount(); i++)
		{
			var childPanel = panel.GetChild(i);
			var in_whitelist = false;
			for (var s = 0; s < whitelist.length; s++)
			{
				if (whitelist[s] == childPanel.id)
				{
					in_whitelist = true;
					break;
				}
			}
			if (!in_whitelist)
			{
				childPanel.SetHasClass("HUD_collapse", !bool);
			}
		}
	}

	GameUI.CustomUIConfig().GetHUDSeed = function()
	{
		var ParentPanel = $.GetContextPanel();
		while(ParentPanel.paneltype != "DOTAHud")
		{
			ParentPanel = ParentPanel.GetParent();
		}
		// $.Msg(panel.GetPositionWithinWindow());
		var width = Game.GetScreenWidth();
		var seed = 1920/width;
		if (ParentPanel.BHasClass("AspectRatio4x3"))
		{
			seed = 1440/width;
		}
		if (ParentPanel.BHasClass("AspectRatio5x4"))
		{
			seed = 1344/width;
		}
		if (ParentPanel.BHasClass("AspectRatio16x10"))
		{
			seed = 1728/width;
		}
		return seed;
	}
	GameUI.CustomUIConfig().CorrectPositionValue = function(value)
	{
		return GameUI.CustomUIConfig().GetHUDSeed() * value;
	}

	GameUI.CustomUIConfig().DistanceBetweenUnits = function(unitindex1,unitindex2)
	{
		if (Entities.IsValidEntity(unitindex1) && Entities.IsValidEntity(unitindex2))
		{
			var position1 = Entities.GetAbsOrigin(unitindex1);
			var position2 = Entities.GetAbsOrigin(unitindex2);
			return Math.sqrt((position2[0]-position1[0])*(position2[0]-position1[0])+(position2[1]-position1[1])*(position2[1]-position1[1]));
		}
		return 0;
	}

	var offsetX = null;
	var offsetY = null;
	var Draggable = false;
	var DragPanel = null;
	function DragCallback()
	{
		var isLeftPressed = GameUI.IsMouseDown(0);
		if (isLeftPressed)
		{
			if (offsetX == null && offsetY == null)
			{
				offsetX = DragPanel.GetPositionWithinWindow().x - (GameUI.GetCursorPosition())[0];
				offsetY = DragPanel.GetPositionWithinWindow().y - (GameUI.GetCursorPosition())[1];
			}
			DragPanel.style.marginLeft = "0px";
			DragPanel.style.marginTop = "0px";
			DragPanel.style.marginRight = "0px";
			DragPanel.style.marginBottom = "0px";
			DragPanel.style.horizontalAlign = "left";
			DragPanel.style.verticalAlign = "top";
			DragPanel.style.x = GameUI.CustomUIConfig().CorrectPositionValue((GameUI.GetCursorPosition())[0] + offsetX) + "px";
			DragPanel.style.y = GameUI.CustomUIConfig().CorrectPositionValue((GameUI.GetCursorPosition())[1] + offsetY) + "px";
		}
		else
		{
			offsetX = null;
			offsetY = null;
		}
		if (Draggable || isLeftPressed)
		{
			$.Schedule(0, DragCallback);
		}
		else
		{
			DragPanel = null;
		}
	}
	GameUI.CustomUIConfig().StartDrag = function(panel)
	{
		Draggable = true;
		DragPanel = panel;
		$.Schedule(0, DragCallback);
	}
	GameUI.CustomUIConfig().EndDrag = function()
	{
		Draggable = false;
	}

	var SoundIndex = null;
	GameUI.CustomUIConfig().SetBGM = function(soundname)
	{
		GameUI.CustomUIConfig().StopBGM();
		SoundIndex = Game.EmitSound(soundname);
	}
	GameUI.CustomUIConfig().StopBGM = function()
	{
		if (SoundIndex != null)
		{
			Game.StopSound(SoundIndex);
			SoundIndex = null;
		}
	}

	function EmitSound(keys)
	{
		if (keys.SoundName)
		{
			Game.EmitSound(keys.SoundName);
		}
	}
	GameEvents.Subscribe("emit_sound", EmitSound);

	GameUI.CustomUIConfig().ErrorMessage = function(msg)
	{
		GameEvents.SendEventClientSide("dota_hud_error_message", {"splitscreenplayer":Players.GetLocalPlayer(), "reason":80, "message":msg});
	}

	GameUI.CustomUIConfig().GetUnitParentEntity = function(unit)
	{
		return CustomNetTables.GetTableValue("unitEntities", unit);
	}

	GameUI.CustomUIConfig().GetContainerByIndex = function(index)
	{
		return CustomNetTables.GetTableValue("itemContainers", index);
	}

	GameUI.CustomUIConfig().GetItemDataByIndex = function(index)
	{
		return CustomNetTables.GetTableValue("items", index);
	}

})();