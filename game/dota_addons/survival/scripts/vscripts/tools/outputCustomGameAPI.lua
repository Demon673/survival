
function OutputCustomGameAPI(  )
	local text = 'window.newCustomGameAPI = {\n'

	function SortedKeys( tbl )
		local result = {}
		if tbl ~= nil then
			for k,_ in pairs( tbl ) do table.insert( result, k ) end
		end
		table.sort( result )
		return result
	end

	function CreateObj(_FDesc, className)
		text = text .. '"' .. className ..'": [\n'
		for _,k in pairs(SortedKeys(_FDesc)) do
			v = _FDesc[k]
			text = text .. "\t{\n"
			-- local t = tostring(v)
			text = text .. '\t\t"name": "'..k..'",\n'
			text = text .. '\t\t"desc": "'..string.gsub(tostring(v.desc),'\n','')..'",\n'
			text = text .. '\t\t"params": [\n'
			local parameterList = {}
			for i = 0, #v-1 do
				local prmType, prmName = unpack( v[i] )
				if prmName == nil or prmName == "" then prmName = string.format( "%s_%d", prmType, i+1 ) end
				text = text .. string.format( '\t\t\t"@param %s %s",\n', prmType, prmName)
				table.insert( parameterList, prmName )
			end
			text = text .. '\t\t],\n'
			text = text .. '\t\t"func": "'..string.format( "function %s( %s ) end", k, table.concat( parameterList, ", " ) )..'",\n'
			text = text .. '\t\t"return": "'..string.format( "@return %s", v.returnType )..'",\n'
			text = text .. "\t},\n"
		end
		text = text .. "],\n"
	end

	CreateObj(FDesc,'Globals');

	for _,className in pairs(SortedKeys(CDesc)) do
		thisClass = CDesc[className]

		CreateObj(thisClass.FDesc,className);
	end

	for _,className in pairs(SortedKeys(EDesc)) do
		thisClass = EDesc[className]

		local const = {}
		for k,v in pairs(thisClass) do
			table.insert(const,{name=k,value=_G[k]})
		end
		table.sort(const,function (a,b)
			return a.value < b.value
		end)

		text = text .. '"Constant_' .. className ..'": [\n'
		for _,t in pairs(const) do
			text = text .. "  {\n"
			text = text .. '    "name": "'..t.name..'",\n'
			text = text .. '    "value": "'..t.value..'",\n'
			text = text .. '    "desc": "'..thisClass[t.name]..'",\n'
			text = text .. "  },\n"
		end
		text = text .. "],\n"
	end

	text = text .. "}\n"
	local out = io.open("d:/dota2_lua_api.txt","w")
    if out then
        out:write(text)
        out:close()
    end
end