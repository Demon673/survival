if cut_tree == nil then cut_tree = class({}) end
--------------------------------------------------------------------------------
function cut_tree:GetCastAnimation()
	return ACT_DOTA_ATTACK
end

function cut_tree:GetCastRange(vLocation, hTarget)
	return 128
end

function cut_tree:OnSpellStart()
	local caster = self:GetCaster():GetParentEntity()
	self.tree = self:GetCursorTarget():GetParentEntity()
	caster:RemoveGesture(ACT_DOTA_ATTACK)
	caster:StartGestureWithPlaybackRate(ACT_DOTA_ATTACK, 0.8)
end

function cut_tree:OnChannelThink(flInterval)
	if self.tree:IsRemove() then
		self:EndChannel(true)
	end
end

function cut_tree:OnChannelFinish(bInterrupted)
	local caster = self:GetCaster():GetParentEntity()
	if not bInterrupted then
		if caster:IsHero() then
			local weapon = caster:GetEquipments():GetItemInSlot(EQUIPMENTS_SLOT_PRIMARY_WEAPON)
			if weapon:IsFiniteUses() then
				weapon:OnUsed("CutTree")
			end
		end
		self.tree:Cut()
		if not self:IsNull() and not self.tree:IsRemove() then
			ExecuteOrderFromTable(
				{
					UnitIndex = caster:GetUnit():entindex(),
					OrderType = DOTA_UNIT_ORDER_CAST_TARGET_TREE,
					TargetIndex = self.tree:GetTreeId(),
					AbilityIndex = self:entindex(),
					Queue = 0,
				}
			)
		end
	else
		caster:RemoveGesture(ACT_DOTA_ATTACK)
	end
end