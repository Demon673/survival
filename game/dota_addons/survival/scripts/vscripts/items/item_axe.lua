axe = {}

function axe:Load()
	LinkLuaModifier("modifier_item_axe", "modifiers/items/axe/modifier_item_axe", LUA_MODIFIER_MOTION_NONE)

    -----------------
	self:AddComponent("equipment")
	self:SetEquipmentsSlot(EQUIPMENTS_SLOT_PRIMARY_WEAPON)
	self.OnEquip = function(self, unit)
		unit:SetCanCutTrees(true)
		self.equipModifier = unit:AddNewModifier(unit, self:GetItem(), "modifier_item_axe", nil)
	end
	self.OnUnequip = function(self, unit)
		unit:SetCanCutTrees(false)
		if self.equipModifier then
			self.equipModifier:Destroy()
		end
	end

    -----------------
	self:AddComponent("finite_uses")
	self:SetConsumption("CutTree", 1)
	self:SetConsumption("Attack", 1)
	self.OnExhausted = function(self)
		self:Remove()
	end
end

return axe