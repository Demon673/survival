return {
    item_twig = require("items/item_twig"),
    item_axe = require("items/item_axe"),
    item_spear = require("items/item_spear"),
    item_branches = require("items/item_branches"),
}
