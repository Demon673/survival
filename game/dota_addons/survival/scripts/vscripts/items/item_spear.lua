spear = {}

function spear:Load()
	LinkLuaModifier("modifier_item_spear", "modifiers/items/spear/modifier_item_spear", LUA_MODIFIER_MOTION_NONE)

    -----------------
	self:AddComponent("equipment")
	self:SetEquipmentsSlot(EQUIPMENTS_SLOT_PRIMARY_WEAPON)

    -----------------
	self:AddComponent("finite_uses")
	self.OnFinished = function(self)
		self:Remove()
	end
end

return spear