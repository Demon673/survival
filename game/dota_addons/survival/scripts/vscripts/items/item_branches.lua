branches = {}

function branches:Load()
    -----------------
	self:AddComponent("stackable")

    -----------------
	self:AddComponent("perishable")
    self:StartPerishing(30)
	self.OnPerish = function(self)
		self:Remove()
	end
end

return branches