Tree = Tree or class({}, nil, Entity)

function Tree:constructor(origin, angles, scale, model, skin, health)
	getbase(Tree).constructor(self)

	local position = GetGroundPosition(origin, nil)

	CreateTempTree(position, 99999)
	local trees = GridNav:GetAllTreesAroundPoint(position, 0, true)
	for i,tree in pairs(trees) do
		self:SetTree(tree)
		self:SetOrigin(position)
		self:SetAngles(angles)
		self:SetScale(scale)
		self:SetModel(model)
		self:SetSkin(skin or "0")
		tree:SetBodygroup(0, 1)

		self:SetHealth(health or 15)
		break
	end
end

function Tree:SetTree(tree)
	self.tree = tree

	self.tree.GetParentEntity = function(tree)
		return self
	end
end

function Tree:GetTree()
	return self.tree
end

function Tree:GetTreeId()
	return GetTreeIdForEntityIndex(self:GetTree():entindex())
end

function Tree:SetOrigin(origin)
	self.origin = origin
	self:GetTree():SetAbsOrigin(self.origin)
end

function Tree:GetOrigin()
	return self.origin
end

function Tree:SetAngles(angles)
	self.angles = angles
	self:GetTree():SetAngles(self.angles.x, self.angles.y, self.angles.z) 
end

function Tree:GetAngles()
	return self.angles
end

function Tree:SetScale(scale)
	self.scale = scale
	self:GetTree():SetModelScale(self.scale)
end

function Tree:GetScale()
	return self.scale
end

function Tree:SetModel(model)
	self.model = model
	self:GetTree():SetModel(self.model)
end

function Tree:GetModel()
	return self.model
end

function Tree:SetSkin()
	self.skin = skin
	self:GetTree():SetMaterialGroup(self.skin)
end

function Tree:GetSkin()
	return self.skin
end

function Tree:SetHealth(health)
	self.health = health
end

function Tree:GetHealth()
	return self.health
end

function Tree:Cut()
	self.tree:ResetSequence("grow1")
	local particleId = ParticleManager:CreateParticle("particles/world_destruction_fx/treeleaves_large.vpcf", PATTACH_ABSORIGIN_FOLLOW, self.tree)
	ParticleManager:ReleaseParticleIndex(particleId)

	self:SetHealth(self:GetHealth()-1)
	if self:GetHealth() == 0 then
		self:CutDown()
	end
end

function Tree:CutDown()
	--掉落

	self:Remove()
end

function Tree:Remove()
	GridNav:DestroyTreesAroundPoint(self.origin, 0, true)

	getbase(Tree).Remove(self)
end