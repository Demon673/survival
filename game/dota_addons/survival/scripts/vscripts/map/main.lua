Map = Map or class({})

require("map/fixed_tree")

Map.Trees = Map.Trees or {}
function Map:CreateTree(origin, angles, scale, model, skin, health)
	local tree = Tree(origin, angles, scale, model, skin, health)
	table.insert(Map.Trees, tree)

	return tree
end

function Map:Init()
	Map:CreateFixedTree()
end