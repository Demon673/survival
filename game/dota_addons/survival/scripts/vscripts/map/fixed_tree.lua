function Map:CreateFixedTree()
	--Map:CreateTree(origin, angles, scale, model[, skin, health]) 注：[]是可以不填的，skin默认"0"，health默认15
	Map:CreateTree(Vector(419.837, -677.493, 129), Vector(0, 0, 0), 1, "models/props_tree/tree_pine_01.vmdl")
	Map:CreateTree(Vector(100, -677.493, 129), Vector(0, 0, 0), 1, "models/props_tree/tree_pine_01.vmdl")
	Map:CreateTree(Vector(100, -100, 129), Vector(0, 0, 0), 1, "models/props_tree/tree_pine_01.vmdl")
	Map:CreateTree(Vector(200, 0, 129), Vector(0, 0, 0), 1, "models/props_tree/tree_pine_01.vmdl")
	Map:CreateTree(Vector(0, 100, 129), Vector(0, 0, 0), 1, "models/props_tree/tree_pine_01.vmdl")
	Map:CreateTree(Vector(0, 0, 129), Vector(0, 0, 0), 1, "models/props_tree/tree_pine_01.vmdl")
	Map:CreateTree(Vector(200, 100, 129), Vector(0, 0, 0), 1, "models/props_tree/tree_pine_01.vmdl")
	Map:CreateTree(Vector(254, -545, 129), Vector(0, 0, 0), 1, "models/props_tree/tree_pine_01.vmdl")
end