Entity = Entity or class({})
Entity.entities = Entity.entities or {}

function Entity:constructor()
	local i = 1
	while Entity.entities[i] ~= nil do
		i = i + 1
	end
	Entity.entities[i] = self
	self.index = i
	self.isRemove = false
end

function Entity:GetIndex()
	return self.index
end

function Entity:Remove()
	Entity.entities[self.index] = nil
	self.isRemove = true
end

function Entity:IsRemove()
	return self.isRemove
end

function GetEntityByIndex(index)
	local self = Entity.entities[index]
	return self
end