Stackable = {}

function Stackable:Activate()
    self.isStackable = true
    self.maxCharges = 30
    self:GetItem():SetCurrentCharges(1)
	self:SetNetTable("isStackable", 1)
end

function Stackable:GetCurrentCharges()
	return self:GetItem():GetCurrentCharges()
end

function Stackable:SetCurrentCharges(iCharges, perishRemainingTime)
	local new = math.min(iCharges, self:GetMaxCharges())
	if self:IsPerishable() and perishRemainingTime ~= nil then
		local add = new - self:GetCurrentCharges()
		if add > 0 then
			self:Dilute(add, perishRemainingTime)
		end
	end

	self:GetItem():SetCurrentCharges(new)

	return math.max(iCharges - self:GetMaxCharges(), 0)
end

function Stackable:GetMaxCharges()
	return self.maxCharges or 0
end

function Stackable:SetMaxCharges(iCharges)
	self.maxCharges = iCharges
end

return Stackable