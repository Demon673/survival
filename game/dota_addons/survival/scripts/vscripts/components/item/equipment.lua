Equipment = {}

function Equipment:Activate()
	self.isEquipment = true
	self:SetNetTable("isEquipment", 1)
end

function Equipment:GetEquipmentsSlot()
	return self.equipmentsSlot or EQUIPMENTS_SLOT_NONE
end

function Equipment:SetEquipmentsSlot(slotType)
	self.equipmentsSlot = slotType
	self:SetNetTable("equipmentsSlot", self.equipmentsSlot)
end

return Equipment