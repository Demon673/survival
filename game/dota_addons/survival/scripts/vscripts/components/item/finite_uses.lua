FiniteUses = {}

function FiniteUses:Activate()
	self.isFiniteUses = true
	self.finiteusesConsumption = {}
	self.finiteusesTotal = 100
	self.finiteusesCurrent = 100
	self:SetNetTable("isFiniteUses", 1)
	self:SetNetTable("finiteusesConsumption", self.finiteusesConsumption)
	self:SetNetTable("finiteusesTotal", self.finiteusesTotal)
	self:SetNetTable("finiteusesCurrent", self.finiteusesCurrent)
end

function FiniteUses:SetConsumption(action, uses)
	self.finiteusesConsumption[action] = uses
	self:SetNetTable("finiteusesConsumption", self.finiteusesConsumption)
end

function FiniteUses:SetMaxUses(val)
	self.finiteusesTotal = val
	self:SetNetTable("finiteusesTotal", self.finiteusesTotal)
end

function FiniteUses:SetUses(val)
	local was_positive = self.finiteusesCurrent > 0
	self.finiteusesCurrent = val
	self:SetNetTable("finiteusesCurrent", self.finiteusesCurrent)

	if self.finiteusesCurrent <= 0 then
		self.finiteusesCurrent = 0
		if was_positive and self.OnExhausted ~= nil then
			self.OnExhausted(self)
		end
	end
end

function FiniteUses:GetUses()
	return self.finiteusesCurrent
end

function FiniteUses:Use(num)
	self:SetUses(self.finiteusesCurrent - (num or 1))
end

function FiniteUses:OnUsed(action)
	local uses = self.finiteusesConsumption[action]
	if uses ~= nil then
		self:Use(uses)
	end
end

function FiniteUses:GetPercent()
	return self.finiteusesCurrent / self.finiteusesTotal
end

function FiniteUses:SetPercent(amount)
	local target = (self.finiteusesTotal * amount)
	self:SetUses((target - self.finiteusesCurrent) + self.finiteusesCurrent)
end

return FiniteUses