Perishable = {}

PERISHING_PERIOD = 0.05

function Perishable:Activate()
	self.isPerishable = true

	self.perishTime = 0
	self.perishRemainingTime = 0

	self:SetNetTable("isPerishable", 1)
	self:SetNetTable("perishTime", self.perishTime)
	self:SetNetTable("perishRemainingTime", self.perishRemainingTime)
end

function Perishable:Dilute(number, timeleft)
	if self:IsStackable() then
		self.perishRemainingTime = (self:GetMaxCharges()*self.perishRemainingTime+number*timeleft) / (self:GetMaxCharges()+number)
		self:SetNetTable("perishRemainingTime", self.perishRemainingTime)
	end
end

function Perishable:StartPerishing(time)
	self.perishTime = time
	self.perishRemainingTime = time
	self:SetNetTable("perishTime", self.perishTime)
	self:SetNetTable("perishRemainingTime", self.perishRemainingTime)

	local lastTime = GameRules:GetGameTime()
	self.perishContextName = DoUniqueString("Perishing")
	GameRules:GetGameModeEntity():SetContextThink(self.perishContextName,
		function()
			if self:IsRemove() then
				return
			end
			if GameRules:GetGameTime() - lastTime > PERISHING_PERIOD then
				lastTime = GameRules:GetGameTime()

				self.perishRemainingTime = math.max(0, self.perishRemainingTime - PERISHING_PERIOD)
				self:SetNetTable("perishRemainingTime", self.perishRemainingTime)

				if self.perishRemainingTime == 0 then
					self:Perish()
					return
				end
			end
			return PERISHING_PERIOD
		end
	, PERISHING_PERIOD)
end

function Perishable:Perish()
	if self.OnPerish ~= nil then
		self.OnPerish(self)
	end

	if self.onperishreplacement then
	end
end

return Perishable