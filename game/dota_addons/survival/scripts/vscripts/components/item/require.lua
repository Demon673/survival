return {
    stackable = require("components/item/stackable"),
    equipment = require("components/item/equipment"),
    finite_uses = require("components/item/finite_uses"),
    perishable = require("components/item/perishable"),
}