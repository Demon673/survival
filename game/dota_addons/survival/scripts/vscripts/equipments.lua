Equipments = Equipments or class({}, nil, ItemContainer)

EQUIPMENTS_SLOT_NONE = 0
EQUIPMENTS_SLOT_PRIMARY_WEAPON = 1		--主武器栏
EQUIPMENTS_SLOT_SECONDARY_WEAPON = 2	--副武器栏
EQUIPMENTS_SLOT_HEAD = 3				--头部栏
EQUIPMENTS_SLOT_UPPER_BODY = 4			--上身栏
EQUIPMENTS_SLOT_LOWER_BODY = 5			--下身栏
EQUIPMENTS_SLOT_FOOT = 6				--脚部栏
EQUIPMENTS_SLOT_COUNT = 6

function Equipments:constructor(owner)
	getbase(Equipments).constructor(self, owner, 1, EQUIPMENTS_SLOT_COUNT)
end

function Equipments:SetItemInSlot(item, i)
	if self.list[i] ~= nil then
		if self.list[i].OnUnequip ~= nil then
			self.list[i]:OnUnequip(self:GetOwner())
		end
	end
	getbase(Equipments).SetItemInSlot(self, item, i)
	if item ~= nil then
		if item.OnEquip ~= nil then
			item:OnEquip(self:GetOwner())
		end
	end
end

function Equipments:AddItemCharges(item)
	local itemName = item:GetName()

	local remain = item:GetCurrentCharges()
	if not item:IsEquipment() then
		return remain
	end
	for i = 1, self:GetCapacity(), 1 do
		local owned_item = self:GetItemInSlot(i)
		if owned_item ~= nil then
			if item:IsStackable() and owned_item:GetName() == itemName then
				remain = owned_item:SetCurrentCharges(owned_item:GetCurrentCharges()+remain, item.perishRemainingTime)
				if remain == 0 then
					item:Remove()
					return 0
				else
					item:SetCurrentCharges(remain)
				end
			end
		end
	end

	return remain
end

function Equipments:AddItemToEmptySlot(item)
	if not item:IsEquipment() then
		return false
	end

	local i = item:GetEquipmentsSlot()
	local owned_item = self:GetItemInSlot(i)
	if owned_item == nil then
		self:SetItemInSlot(item, i)
		return true
	end

	return false
end

getbase(Equipments).IsEquipments = function(self)
	return false
end
function Equipments:IsEquipments()
	return true
end