modifier_chest = modifier_chest or class({})

function modifier_chest:CheckState()
	return {
		[MODIFIER_STATE_INVULNERABLE] = true,
		[MODIFIER_STATE_NO_HEALTH_BAR] = true,
		[MODIFIER_STATE_ROOTED] = true,
	}
end

function modifier_chest:DeclareFunctions()
	return {

    }
end

function modifier_chest:IsHidden()
	return true
end

function modifier_chest:IsPurgable()
	return false
end

function modifier_chest:IsPurgeException()
	return false
end