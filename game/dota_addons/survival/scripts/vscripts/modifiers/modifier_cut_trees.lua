modifier_cut_trees = modifier_cut_trees or class({})

function modifier_cut_trees:OnCreated(params)
	if IsServer() then
		self.cut_tree = self:GetParent():AddAbility("cut_tree")
		self.cut_tree:SetLevel(1)
		self.cut_tree:SetHidden(true)
		self:GetParent():SwapAbilities("empty_1", "cut_tree", false, true)
	end
end

function modifier_cut_trees:OnDestroy()
	if IsServer() then
		self:GetParent():SwapAbilities("empty_1", "cut_tree", true, false)
		self:GetParent():RemoveAbility("cut_tree")
	end
end

function modifier_cut_trees:DeclareFunctions()
	return { MODIFIER_PROPERTY_CAN_ATTACK_TREES }
end

function modifier_cut_trees:GetModifierCanAttackTrees()
	return 1
end

function modifier_cut_trees:IsHidden()
	return true
end

function modifier_cut_trees:IsPurgable()
	return false
end

function modifier_cut_trees:IsPurgeException()
	return false
end