modifier_item_axe = modifier_item_axe or class({})

function modifier_item_axe:IsHidden()
	return true
end

function modifier_item_axe:IsPurgable()
	return false
end

function modifier_item_axe:IsPurgeException()
	return false
end

function modifier_item_axe:OnCreated(params)
    self.bonus_base_damage = 25
end

function modifier_item_axe:DeclareFunctions()
	return {
        MODIFIER_PROPERTY_BASEATTACK_BONUSDAMAGE
    }
end

function modifier_item_axe:GetModifierBaseAttack_BonusDamage(params)
	return self.bonus_base_damage
end